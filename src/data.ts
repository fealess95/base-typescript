export const DATA = {
  uniqueId: 999,
  name: "Mark Zuckerberg",
  subordinates: [
    {
      uniqueId: 1,
      name: "Sarah Donald",
      subordinates: [
        {
          uniqueId: 11,
          name: "Cassandra Reynolds",
          subordinates: [
            {
              uniqueId: 111,
              name: "Mary Blue",
              subordinates: []
            },
            {
              uniqueId: 112,
              name: "Bob Saget",
              subordinates: [
                {
                  uniqueId: 1121,
                  name: "Tina Teff",
                  subordinates: [
                    {
                      uniqueId: 11211,
                      name: "Will Turner",
                      subordinates: []
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      uniqueId: 2,
      name: "Tyler Simpson",
      subordinates: [
        {
          uniqueId: 21,
          name: "Harry Tobs",
          subordinates: [
            {
              uniqueId: 211,
              name: "Thomas Brown",
              subordinates: []
            }
          ]
        },
        {
          uniqueId: 22,
          name: "Gary Styles",
          subordinates: []
        },
        {
          uniqueId: 23,
          name: "Tyler Simpson",
          subordinates: []
        }
      ]
    },
    {
      uniqueId: 3,
      name: "Bruce Willis",
      subordinates: []
    },
    {
      uniqueId: 4,
      name: "Georgina Flangy",
      subordinates: [
        {
          uniqueId: 41,
          name: "Sophie Turner",
          subordinates: []
        }
      ]
    }
  ]
};
