import Stack from "./stack";
import { Action, Employee, IEmployeeOrgApp } from "./types";

const ACTIONS = {
  UNDO: "undo",
  REDO: "redo"
};

export class EmployeeOrgApp implements IEmployeeOrgApp {
  ceo: Employee;
  private readonly undoStack: Stack<Action>;
  private readonly redoStack: Stack<Action>;
  private readonly undoStorageStack: Stack<Action>;
  private readonly redoStorageStack: Stack<Action>;

  constructor(data: Employee) {
    this.ceo = data;
    this.undoStack = new Stack();
    this.redoStack = new Stack();
    this.undoStorageStack = new Stack();
    this.redoStorageStack = new Stack();
  }

  move(employeeId: number, newSupervisorId: number): void {
    const {
      employee,
      supervisor: oldSupervisor,
      employeeIndex
    } = this.findEmployeeAndSupervisor(employeeId);
    const { employee: newSupervisor } = this.findEmployeeAndSupervisor(
      newSupervisorId
    );

    if (employee && newSupervisor && oldSupervisor && employeeIndex !== null) {
      const employeeSubordinates = [...employee.subordinates];

      this.moveEmployeeToNewSupervisor({
        employee,
        oldSupervisor,
        newSupervisor
      });
      this.redoStack.clear();
      this.addToUndoStack(() =>
        this.moveEmployeeBackToOldSupervisor({
          employee,
          oldSupervisor,
          newSupervisor,
          employeeSubordinates,
          employeeIndex
        })
      );
      this.addToRedoStorageStack(() =>
        this.moveEmployeeToNewSupervisor({
          employee,
          oldSupervisor,
          newSupervisor
        })
      );
    }
  }

  undo(): void {
    if (!this.undoStack.isEmpty()) {
      const undoAction = this.pickUpAction(ACTIONS.UNDO, ACTIONS.UNDO);
      const redoAction = this.pickUpAction(ACTIONS.REDO, ACTIONS.UNDO);
      this.addToUndoStorageStack(undoAction!);
      this.addToRedoStack(redoAction!);
      undoAction!();
    }
  }

  redo(): void {
    if (!this.redoStack.isEmpty()) {
      const redoAction = this.pickUpAction(ACTIONS.REDO, ACTIONS.REDO);
      const undoAction = this.pickUpAction(ACTIONS.UNDO, ACTIONS.REDO);
      this.addToUndoStack(undoAction!);
      this.addToRedoStorageStack(redoAction!);
      redoAction!();
    }
  }

  private pickUpAction(action: string, executor: string): Action | undefined {
    if (action === ACTIONS.UNDO && executor === ACTIONS.UNDO)
      return this.undoStack.pop();

    if (action === ACTIONS.UNDO && executor === ACTIONS.REDO)
      return this.undoStorageStack.pop();

    if (action === ACTIONS.REDO && executor === ACTIONS.REDO)
      return this.redoStack.pop();

    if (action === ACTIONS.REDO && executor === ACTIONS.UNDO)
      return this.redoStorageStack.pop();
  }

  private addToUndoStack(action: Action): void {
    this.undoStack.push(action);
  }

  private addToRedoStack(action: Action): void {
    this.redoStack.push(action);
  }

  private addToUndoStorageStack(action: Action): void {
    this.undoStorageStack.push(action);
  }

  private addToRedoStorageStack(action: Action): void {
    this.redoStorageStack.push(action);
  }

  private moveEmployeeToNewSupervisor({
    employee,
    oldSupervisor,
    newSupervisor
  }: {
    employee: Employee;
    oldSupervisor: Employee;
    newSupervisor: Employee;
  }): void {
    // Copy all subordinates at the same level as the employee's position
    oldSupervisor.subordinates.push(...employee.subordinates);

    // Delete employee subordinates
    employee.subordinates = [];

    // Push the employee to the new supervisor's subordinate list
    newSupervisor.subordinates.push(employee);

    // Remove the employee from the old supervisor's subordinate list
    oldSupervisor.subordinates = oldSupervisor.subordinates.filter(
      sub => sub.uniqueId !== employee.uniqueId
    );
  }

  private moveEmployeeBackToOldSupervisor({
    employee,
    oldSupervisor,
    newSupervisor,
    employeeSubordinates,
    employeeIndex
  }: {
    employee: Employee;
    oldSupervisor: Employee;
    newSupervisor: Employee;
    employeeSubordinates: Employee["subordinates"];
    employeeIndex: number;
  }): void {
    // Remove the employee from new supervisor
    newSupervisor.subordinates = newSupervisor.subordinates.filter(
      sub => sub.uniqueId !== employee.uniqueId
    );

    // Move the employee back to the old supervisor
    oldSupervisor.subordinates.splice(employeeIndex, 0, employee);

    // Add employee subordinates
    employee.subordinates.push(...employeeSubordinates);

    // Remove the employee subordinates from old supervisor
    oldSupervisor.subordinates = oldSupervisor.subordinates.filter(
      sub =>
        !employeeSubordinates
          .map(employeeSubordinate => employeeSubordinate.uniqueId)
          .includes(sub.uniqueId)
    );
  }

  private findEmployeeAndSupervisor(
    employeeId: number,
    node: Employee = this.ceo,
    parent: Employee | null = null
  ): {
    employee: Employee | null;
    supervisor: Employee | null;
    employeeIndex: number | null;
  } {
    if (node.uniqueId === employeeId)
      return {
        employee: node,
        supervisor: parent,
        employeeIndex: parent ? parent.subordinates.indexOf(node) : null
      };

    for (const subordinate of node.subordinates) {
      const result = this.findEmployeeAndSupervisor(
        employeeId,
        subordinate,
        node
      );
      if (result.employee) return result;
    }

    return { employee: null, supervisor: null, employeeIndex: null };
  }
}
