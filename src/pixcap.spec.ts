import { EmployeeOrgApp } from "./pixcap";
import { DATA } from "./data";

describe("EmployeeOrgApp", () => {
  it("work correctly", () => {
    const app = new EmployeeOrgApp(DATA);
    app.move(4, 3);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 112,
                name: "Bob Saget",
                subordinates: [
                  {
                    uniqueId: 1121,
                    name: "Tina Teff",
                    subordinates: [
                      {
                        uniqueId: 11211,
                        name: "Will Turner",
                        subordinates: []
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      }
    ]);
    app.undo();
    expect(app.ceo).toEqual(DATA);
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 112,
                name: "Bob Saget",
                subordinates: [
                  {
                    uniqueId: 1121,
                    name: "Tina Teff",
                    subordinates: [
                      {
                        uniqueId: 11211,
                        name: "Will Turner",
                        subordinates: []
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      }
    ]);

    app.move(11211, 112);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 112,
                name: "Bob Saget",
                subordinates: [
                  {
                    uniqueId: 1121,
                    name: "Tina Teff",
                    subordinates: []
                  },
                  {
                    uniqueId: 11211,
                    name: "Will Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      }
    ]);

    app.move(112, 999);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: []
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.undo();
    app.undo();
    app.undo();
    expect(app.ceo).toEqual(DATA);
    app.undo();
    expect(app.ceo).toEqual(DATA);
    app.redo();
    app.redo();
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: []
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: []
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 22,
            name: "Gary Styles",
            subordinates: []
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);

    app.move(22, 11211);
    app.move(41, 11211);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.undo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 41,
        name: "Sophie Turner",
        subordinates: []
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);

    app.move(41, 11211);
    app.move(11211, 2);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 22,
                name: "Gary Styles",
                subordinates: []
              },
              {
                uniqueId: 41,
                name: "Sophie Turner",
                subordinates: []
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          },
          {
            uniqueId: 11211,
            name: "Will Turner",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.redo();
    app.redo();
    app.redo();
    app.redo();
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 22,
                name: "Gary Styles",
                subordinates: []
              },
              {
                uniqueId: 41,
                name: "Sophie Turner",
                subordinates: []
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          },
          {
            uniqueId: 11211,
            name: "Will Turner",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.undo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);

    // START: unexpected case: pass wrong ids
    app.move(3, 999999);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.move(888888, 1);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.move(888888, 999999);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    // END: unexpected case: pass wrong ids

    app.move(3, 1);
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          },
          {
            uniqueId: 3,
            name: "Bruce Willis",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      },
      {
        uniqueId: 4,
        name: "Georgina Flangy",
        subordinates: []
      }
    ]);
    app.undo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 3,
        name: "Bruce Willis",
        subordinates: [
          {
            uniqueId: 4,
            name: "Georgina Flangy",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      }
    ]);
    app.redo();
    expect(app.ceo.subordinates).toEqual([
      {
        uniqueId: 1,
        name: "Sarah Donald",
        subordinates: [
          {
            uniqueId: 11,
            name: "Cassandra Reynolds",
            subordinates: [
              {
                uniqueId: 111,
                name: "Mary Blue",
                subordinates: []
              },
              {
                uniqueId: 1121,
                name: "Tina Teff",
                subordinates: []
              },
              {
                uniqueId: 11211,
                name: "Will Turner",
                subordinates: [
                  {
                    uniqueId: 22,
                    name: "Gary Styles",
                    subordinates: []
                  },
                  {
                    uniqueId: 41,
                    name: "Sophie Turner",
                    subordinates: []
                  }
                ]
              }
            ]
          },
          {
            uniqueId: 3,
            name: "Bruce Willis",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 2,
        name: "Tyler Simpson",
        subordinates: [
          {
            uniqueId: 21,
            name: "Harry Tobs",
            subordinates: [
              {
                uniqueId: 211,
                name: "Thomas Brown",
                subordinates: []
              }
            ]
          },
          {
            uniqueId: 23,
            name: "Tyler Simpson",
            subordinates: []
          }
        ]
      },
      {
        uniqueId: 112,
        name: "Bob Saget",
        subordinates: []
      },
      {
        uniqueId: 4,
        name: "Georgina Flangy",
        subordinates: []
      }
    ]);
  });
});
