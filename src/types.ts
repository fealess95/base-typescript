export type Action = () => void;

export type Employee = {
  uniqueId: number;
  name: string;
  subordinates: Employee[];
};

export type IEmployeeOrgApp = {
  ceo: Employee;
  move(employeeId: number, newSupervisorId: number): void;
  undo(): void;
  redo(): void;
};
